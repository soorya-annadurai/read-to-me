#include <iostream>
#include <conio.h>
#include <fstream>
#include <windows.h>

using namespace std;

const short int rate = -1;


void gotoXY(int x, int y)
{
     COORD CursorPosition;
     CursorPosition.X = x; // Locates column
     CursorPosition.Y = y; // Locates Row
     SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),CursorPosition); // Sets position for next thing to be printed
}

int main()
{
    ifstream text;
    ofstream talk;
    text.open("text.txt", ios::in);
    string word[100];
    char c;
    char trigger;
    int word_count = 0;
    int current_set_count = 0;
    while (!text.eof())
    {
        text.get(c);
        if (text.eof())
            break;
        if ((int)c >= 32 && (int)c <= 255 && c != ' ')
            word[word_count] += c;
        else
            word_count++;
    }
    cout << endl;

    /*
    cout << "Word list: " << endl;
    for (int i = 0; i <= word_count; i++)
    {
        cout << word[i] << endl;
    }
    */
    int word_pos = 0;
    bool hit = FALSE;
    cout << "Press any key to start reading." << endl;

    getch();

    cout << endl << "Trying to read the words." << endl << endl;
    while (word_pos < word_count)
    {
        //cout << "count: " << word_count << endl << "pos: " << word_pos << endl;
        current_set_count = 0;
        hit = FALSE;
        talk.open("talk.vbs", ios::out | ios::trunc);
        talk.seekp(0);
        talk << "message=";
        talk << "\"";

        while (!kbhit())
        {

            if (word_pos <= word_count)
            {
                Sleep(100);
                hit = TRUE;
                talk << word[word_pos] << " ";
                current_set_count++;
                word_pos++;
                gotoXY(0,5);
                cout << "Words to read:" << endl; // now, 5th line.
                gotoXY(0,6);
                cout << "     ";
                gotoXY(0,6);
                cout << current_set_count;
                if (word_pos > word_count)
                {
                    break;
                }
            }
        }
        //talk << word[i] << " ";

        talk << "\"";
        talk << "\n";
        talk << "Set sapi=CreateObject(\"sapi.spvoice\")";
        talk << "\n";
        talk << "sapi.Rate = ";
        talk << rate;
        talk << "\n";
        talk << "sapi.Speak message";
        talk.close();

        batch:

        if (hit == TRUE)
            system("talk.vbs");

        getch();

        gotoXY(0,8);
        cout << "Press any key to keep reading. x to exit, r to repeat the last phrase.";
        trigger = getche();
        gotoXY(0,8);
        cout << "                                                                                   ";
        if (trigger == 'x')
        {
            break;
        }
        else if (trigger == 'r')
        {
            goto batch;
        }
    }
    remove ("talk.vbs");

}
